import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

// Firebase
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from '../../node_modules/angularfire2';
import { environment } from '../environments/environment';

// Components
import { SheetsComponent } from './components/sheets/sheets.component';
import { SheetsListComponent } from './components/sheets/sheets-list/sheets-list.component';
import { SheetComponent } from './components/sheets/sheet/sheet.component';

// Services
import { SheetService } from './services/sheet.service';

import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    SheetsComponent,
    SheetsListComponent,
    SheetComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [
    SheetService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
