import { Component, OnInit } from '@angular/core';

// Services
import { SheetService } from 'src/app/services/sheet.service';

// Model
import { Sheet } from 'src/app/models/sheet';

// Toastr
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-sheets-list',
  templateUrl: './sheets-list.component.html',
  styleUrls: ['./sheets-list.component.css']
})
export class SheetsListComponent implements OnInit {

  sheetList: Sheet[];

  constructor(private sheetService: SheetService, private toastr: ToastrService) { }

  ngOnInit() {
    this.sheetService.getSheets()
    .snapshotChanges()
    .subscribe(item => {
      this.sheetList = [];
      item.forEach(element => {
        let x = element.payload.toJSON();
        x['$key'] = element.key;
        this.sheetList.push(x as Sheet);
      });
    });
  }

  onEdit(sheet: Sheet) {
    this.sheetService.selectedSheet = Object.assign({}, sheet);
  }

  onDelete(sheet: Sheet) {
    if (confirm('Are you sure')) {
      this.sheetService.deleteSheet(sheet);
      this.toastr.success('Deleted', 'Delete');
    }
  }

}
