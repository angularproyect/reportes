import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Sheet } from '../models/sheet';


@Injectable({
  providedIn: 'root'
})
export class SheetService {

  sheetsList: AngularFireList<any>;
  selectedSheet: Sheet = new Sheet();

  constructor(private firebase: AngularFireDatabase) { }

  getSheets() {
    return this.sheetsList = this.firebase.list('sheet');
  }

  insertSheet(sheet: Sheet) {
    // console.log(JSON.stringify(sheet));
    this.sheetsList.push(
      {
        nombre: sheet.nombre,
        birthday: sheet.birthday
      }
  );
  }

  updateSheet(sheet: Sheet) {
    this.sheetsList.update(sheet.$key, {
      nombre: sheet.nombre,
      birthday: sheet.birthday
    });
  }

  deleteSheet(sheet: Sheet) {
    this.sheetsList.remove(sheet.$key);
  }
}
